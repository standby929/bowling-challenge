export class Bowling {

  /**
   * Variable for storing the throws / rolls
   */
  private throws: Array<string> = [];
  private frame = 0;

  /**
   * @param rolls Array of strings containing all the throws / rolls
   * @returns Fills up the throws array with content.
   */
  rolls(rolls: string) {
    const rollArray = rolls.split(' ');
    const rollF1 = rollArray[0];
    const rollF2 = rollArray[1];
    this.throws.push(rollF1);
    if (rollF2) {
      this.throws.push(rollF2);
    }
  }

  /**
   * @returns The calculated total score.
   */
  getScore(): number {
    let score = 0;

    // Going through the 10 frames (thw whole game)
    for (let frameIndex = 0, throwIndex = 0; frameIndex < 10; ++frameIndex) {
      // 1. Increase score with the first roll or throw
      score += this.translateRoll(this.throws[throwIndex], throwIndex);
      // 2. Increase score with the second roll
      score += this.translateRoll(this.throws[throwIndex + 1], throwIndex + 1);

      // 3. If it is a spare or strike, increase the score with the value of the
      // throws array at the throw + 2nd index
      // score in the first point + score in the second point (this is the spare) + score in the third point
      // 4. If it is a strike:
      // score in the first point + next two throws = 1 + 2 + 3
      if (this.isSpare(throwIndex) || this.isStrike(throwIndex)) {
        score += this.translateRoll(this.throws[throwIndex + 2], throwIndex + 2);
      }

      if (this.isStrike(throwIndex)) {
        // if it was a strike you should increase the index of the throw by one
        // because on that frame you can roll only once
        throwIndex++;
      } else {
        // otherwise you should increase the throwindex by 2 to get the next frame
        throwIndex += 2;
      }
    }
    return score;
  }

  /**
   * @param throwIndex The index of the throw, used in this.throws[] array.
   * @returns Whether a throw was a Spare
   */
  private isSpare(throwIndex: number): boolean {
    return this.translateRoll(this.throws[throwIndex], throwIndex) + this.translateRoll(this.throws[throwIndex + 1], throwIndex + 1) === 10;
  }

  /**
   * @param throwIndex The index of the throw, used in this.throws[] array.
   * @returns Whether a throw was a Strike
   */
  private isStrike(throwIndex: number): boolean {
    return this.translateRoll(this.throws[throwIndex], throwIndex) === 10;
  }

  /**
   *
   * @param roll The representation of the roll
   * @param throwIndex Index of the roll in the throws array.
   * @returns Returns with the numeric representation of the roll according to the given parameters, e.g: X = 10.
   */
  private translateRoll(roll: string, throwIndex: number): number {
    let res;
    if (roll.toLowerCase() === 'x') {
      res = +10;
    } else if (roll === '/') {
      // This is a special case, when you roll gutter first and 10 for the second...
      res = +(10 - this.translateRoll(this.throws[throwIndex - 1], throwIndex - 1));
    } else if (roll === '-') {
      res = +0;
    } else {
      res = +roll;
    }

    return res;
  }
}
