import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { Bowling } from './Bowling';

describe('Bowling test cases', () => {

  let bowling: Bowling;

  beforeEach(async(() => {
    bowling = new Bowling();
  }));

  it('All gutters', async(() => {
    for (let i = 0; i < 10; i++) {
      bowling.rolls('- -');
    }
    expect(bowling.getScore()).toBe(0);
  }));

  it('All fours', async(() => {
    for (let i = 0; i < 10; i++) {
      bowling.rolls('4 4');
    }
    expect(bowling.getScore()).toBe(80);
  }));

  it('Spare and all gutters', async(() => {
    bowling.rolls('5 /');
    for (let i = 0; i < 9; i++) {
      bowling.rolls('- -');
    }
    expect(bowling.getScore()).toBe(10);
  }));

  it('Spare and all fours', async(() => {
    bowling.rolls('5 /');
    for (let i = 0; i < 9; i++) {
      bowling.rolls('4 4');
    }
    expect(bowling.getScore()).toBe(86); // 10 + '4' + 18 * '4' = 86
  }));

  it('Strike and all gutters', async(() => {
    bowling.rolls('X');
    for (let i = 0; i < 9; i++) {
      bowling.rolls('- -');
    }
    expect(bowling.getScore()).toBe(10);
  }));

  it('Strike and all fours', async(() => {
    bowling.rolls('X');
    for (let i = 0; i < 9; i++) {
      bowling.rolls('4 4');
    }
    expect(bowling.getScore()).toBe(90); // '10' + 4 + 4 + 18 * 4 = 90
  }));

  it('All spares with 5/5 + bonus 5', async(() => {
    for (let i = 0; i < 10; i++) {
      bowling.rolls('5 /');
    }
    bowling.rolls('5');
    expect(bowling.getScore()).toBe(150);
  }));

  it('All spares + bonus 1: -/ 1/ 2/ 3/ 4/ 5/ 6/ 7/ 8/ 9/ 1', async(() => {
    bowling.rolls('- /');
    bowling.rolls('1 /');
    bowling.rolls('2 /');
    bowling.rolls('3 /');
    bowling.rolls('4 /');
    bowling.rolls('5 /');
    bowling.rolls('6 /');
    bowling.rolls('7 /');
    bowling.rolls('8 /');
    bowling.rolls('9 /');
    bowling.rolls('1');
    expect(bowling.getScore()).toBe(146);
  }));

  it('All spares + bonus strike: -/ 1/ 2/ 3/ 4/ 5/ 6/ 7/ 8/ 9/ X', async(() => {
    bowling.rolls('- /');
    bowling.rolls('1 /');
    bowling.rolls('2 /');
    bowling.rolls('3 /');
    bowling.rolls('4 /');
    bowling.rolls('5 /');
    bowling.rolls('6 /');
    bowling.rolls('7 /');
    bowling.rolls('8 /');
    bowling.rolls('9 /');
    bowling.rolls('X');
    expect(bowling.getScore()).toBe(155);
  }));

  it('All strikes / Perfect round (in your dreams :D)', async(() => {
    for (let i = 0; i < 12; i++) {
      bowling.rolls('X');
    }
    expect(bowling.getScore()).toBe(300);
  }));

});
